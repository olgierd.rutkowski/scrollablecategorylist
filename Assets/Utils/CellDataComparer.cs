﻿using System.Collections;
using System.Collections.Generic;


public class CellDataComparer : IComparer<BaseCellData>
{
	public bool showSubcategories = true;

	public int Compare(BaseCellData x, BaseCellData y)
	{
		CaseInsensitiveComparer comparer = new CaseInsensitiveComparer();
		if (x.CategoryName != y.CategoryName)
			return comparer.Compare(x.CategoryName, y.CategoryName);
		if (
			showSubcategories
			&& x.SubcategoryName.HasValue
			&& y.SubcategoryName.HasValue
			&& (x.SubcategoryName.Value != y.SubcategoryName.Value)
		)
			return comparer.Compare(x.SubcategoryName.Value, y.SubcategoryName.Value);
		return comparer.Compare(x.SortingPhrase(), y.SortingPhrase());
	}
}
