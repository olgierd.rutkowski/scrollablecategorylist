﻿using UnityEngine.UI;


public class UI
{
	static public void ApplyTextData(Text text, TextData data)
	{
        text.text = data.text_;
    }

    static public void ApplyImageData(Image image, ImageData data)
    {
        if (data.image_.HasValue)
            image.sprite = data.image_.Value;
        if (data.color_.HasValue)
            image.color = data.color_.Value;
    }
}
