﻿using System.Collections;
using System.Collections.Generic;


public class SortingMethod
{
	private readonly CellDataComparer comparer_ = new CellDataComparer();

	public List<BaseCellData> Sort(
		List<BaseCellData> data,
		SortingMethodEnum sortingMethod,
		bool showSubcategories
	)
	{
		this.comparer_.showSubcategories = showSubcategories;
		switch(sortingMethod)
		{
			case SortingMethodEnum.Alphabetical:
				return this.SortAlphabetically(data);
			case SortingMethodEnum.ReverseAlphabetical:
				return this.SortReverseAlphabetically(data);
		}
		return data;
	}

	private List<BaseCellData> SortAlphabetically(List<BaseCellData> data)
	{
		data.Sort(this.comparer_);		
		return data;
	}

	private List<BaseCellData> SortReverseAlphabetically(List<BaseCellData> data)
	{
		data.Sort(this.comparer_);
		data.Reverse();
		return data;
	}
}
