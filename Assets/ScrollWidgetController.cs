﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;


public class ScrollWidgetController : MonoBehaviour
{
    public InputField input_ = null;
    public Button scrollTo_ = null;

    private void Start()
    {
        Assert.IsNotNull(
            this.input_,
            "Input field has to be provided for ScrollWidgetController."
        );
        Assert.IsNotNull(
            this.scrollTo_,
            "Scroll to button has to be provided for ScrollWidgetController."
        );

        this.scrollTo_.onClick.AddListener(this.ScrollTo);
    }

    private void ScrollTo()
	{

	}
}
