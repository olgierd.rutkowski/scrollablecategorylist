﻿using UnityEngine;
using UnityEngine.Assertions;


public class TextData
{
	public string text_ = null;

	public TextData(string text)
	{
		Assert.IsFalse(string.IsNullOrEmpty(text), "Text can't be empty in text data.");
		this.text_ = text;
	}
}
