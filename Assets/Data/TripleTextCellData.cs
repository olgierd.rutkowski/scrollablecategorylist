﻿using UnityEngine;


public class TripleTextCellData : BaseCellData
{
	public Optional<TextData> leftTextData_;
	public Optional<TextData> middleTextData_;
	public Optional<TextData> rightTextData_;
	public Optional<ImageData> imageData_;

	public TripleTextCellData(string categoryName, Optional<string> subcategoryName) :
		base(CellTypeEnum.TripleText, categoryName, subcategoryName)
	{}

	public override bool ContainsPhrase(string phrase)
	{
		return base.ContainsPhrase(phrase)
			|| (
				this.leftTextData_.HasValue
				&& this.leftTextData_.Value.text_.ToLower().Contains(phrase)
			)
			|| (
				this.middleTextData_.HasValue
				&& this.middleTextData_.Value.text_.ToLower().Contains(phrase)
			)
			|| (
				this.rightTextData_.HasValue
				&& this.rightTextData_.Value.text_.ToLower().Contains(phrase)
			);
	}

	public override string SortingPhrase()
	{
		return this.rightTextData_.HasValue
			? this.rightTextData_.Value.text_
			: this.middleTextData_.HasValue
				? this.middleTextData_.Value.text_
				: this.leftTextData_.HasValue
					? this.leftTextData_.Value.text_
					: "";
	}
}
