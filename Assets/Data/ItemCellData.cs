﻿

public class ItemCellData : BaseCellData
{
	public TextData rarityTextData_ = null;
	public TextData itemNameTextData_ = null;

	public ItemCellData(string categoryName, string subcategoryName) :
		base(CellTypeEnum.Item, categoryName, subcategoryName)
	{}

	public override bool ContainsPhrase(string phrase)
	{
		return base.ContainsPhrase(phrase)
			|| this.rarityTextData_.text_.ToLower().Contains(phrase)
			|| this.itemNameTextData_.text_.ToLower().Contains(phrase);
	}

	public override string SortingPhrase()
	{
		return this.itemNameTextData_.text_;
	}
}
