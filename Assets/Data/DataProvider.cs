﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;


/// <summary>
/// Interface of a provider of data for the list.
/// Isn't an interface, because it has to derive from Monobehaviour and can't be abstract.
/// </summary>
public abstract class DataProvider : MonoBehaviour
{
	public event DataChanged OnDataChanged;
	public delegate void DataChanged();

	public ListController listController_ = null;
	public InputField searchInput_ = null;
	public Dropdown sortDropdown_ = null;
	public SortingMethodEnum[] possibleSortingMethods_ = null;

	private List<SortingMethodEnum> sortingMethods_ = null;

	private readonly SortingMethod sortingMethod_ = new SortingMethod();

	protected void Start()
	{
		Assert.IsNotNull(
			this.listController_,
			"List controller has to be provided for DataProvider."
		);

		if (this.searchInput_)
		{
			this.searchInput_.onValueChanged.AddListener(this.SearchInputChanged);
		}

		if (this.sortDropdown_ && this.possibleSortingMethods_.Length > 0)
		{
			// Magical +1 is taken from that sorting method None is added by hand.
			this.sortingMethods_ =
				new List<SortingMethodEnum>(this.possibleSortingMethods_.Length + 1);
			this.sortingMethods_.Add(SortingMethodEnum.None);
			foreach (SortingMethodEnum sortingMethod in this.possibleSortingMethods_)
				this.sortingMethods_.Add(sortingMethod);

			this.sortDropdown_.options.Clear();
			foreach (SortingMethodEnum sortingMethod in this.sortingMethods_)
			{
				this.sortDropdown_.options.Add(
					new Dropdown.OptionData(EnumToString.SortingMethodEnumToString(sortingMethod))
				);
			}
			this.sortDropdown_.value = 0;
			this.sortDropdown_.captionText.text =
				EnumToString.SortingMethodEnumToString(SortingMethodEnum.None);
			this.sortDropdown_.onValueChanged.AddListener(this.SortingChanged);
		}
	}

	public List<BaseCellData> ListData()
	{
		return this.sortingMethod_.Sort(
			this.FilterDataByText(
				this.GenerateData(),
				this.searchInput_ ? this.searchInput_.text.ToLower() : string.Empty
			),
			this.sortDropdown_
				? this.sortingMethods_[this.sortDropdown_.value]
				: SortingMethodEnum.None,
			this.listController_.showSubcategories_
		);
	}

	protected abstract List<BaseCellData> GenerateData();

	protected void EmitDataChangedEvent()
	{
		this.OnDataChanged.Invoke();
	}

	private void SearchInputChanged(string _)
	{
		this.EmitDataChangedEvent();
	}

	private void SortingChanged(int _)
	{
		this.EmitDataChangedEvent();
	}

	private List<BaseCellData> FilterDataByText(List<BaseCellData> data, string searchPhrase)
	{
		if (searchPhrase.Length == 0)
			return data;
			

		List<BaseCellData> filteredData = new List<BaseCellData>();
		foreach (BaseCellData cellData in data)
		{
			if (cellData.ContainsPhrase(searchPhrase))
				filteredData.Add(cellData);
		}
		return filteredData;
	}
}
