﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;


public class MockDataProvider : DataProvider
{
	protected override List<BaseCellData> GenerateData()
	{
		List<BaseCellData> listData = new List<BaseCellData>
		{
			new TripleTextCellData("1", "sub 1"),
			new TripleTextCellData("1", "sub 1"),
			new TripleTextCellData("2", "sub 2"),
			new TripleTextCellData("2", new Optional<string>()),
			new TripleTextCellData("3", "sub 3")
		};
		return listData;
	}
}
