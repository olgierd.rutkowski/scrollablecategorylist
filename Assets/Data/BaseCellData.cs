﻿using System;
using UnityEngine.Assertions;


public abstract class BaseCellData
{
	public Optional<ImageData> backgroundData_;

	private CellTypeEnum cellType_ = CellTypeEnum.Unknown;
	public CellTypeEnum CellType => this.cellType_;

	private readonly string categoryName_ = null;
	public string CategoryName => this.categoryName_;

	private readonly Optional<string> subcategoryName_;
	public Optional<string> SubcategoryName => this.subcategoryName_;

	public BaseCellData(
		CellTypeEnum cellType,
		string categoryName,
		Optional<string> subcategoryName
	)
	{
		Assert.AreNotEqual(CellTypeEnum.Unknown, cellType, "Cells can't be of unknown type.");
		this.cellType_ = cellType;

		Assert.IsFalse(
			String.IsNullOrEmpty(categoryName),
			"Category name has to be provided for BaseCellData."
		);
		this.categoryName_ = categoryName;

		Assert.IsTrue(
			!subcategoryName.HasValue || !String.IsNullOrEmpty(subcategoryName.Value),
			"Subcategory can't be empty if provided for BaseCellData."
		);
		this.subcategoryName_ = subcategoryName;
	}

	public bool IsCategoryCell()
	{
		return this.cellType_ != CellTypeEnum.Category
			|| this.cellType_ != CellTypeEnum.Subcategory;
	}

	public virtual bool ContainsPhrase(string phrase)
	{
		return this.categoryName_.ToLower().Contains(phrase)
			|| (
				this.subcategoryName_.HasValue
				&& this.subcategoryName_.Value.ToLower().Contains(phrase)
			)
			|| EnumToString.CellTypeEnumToString(this.cellType_).ToLower().Contains(phrase);
	}

	public abstract string SortingPhrase();
}
