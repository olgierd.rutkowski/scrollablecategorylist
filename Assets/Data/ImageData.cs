﻿using UnityEngine;


public class ImageData
{
	public Optional<Sprite> image_;
	public Optional<Color> color_;
}
