﻿using System.Collections.Generic;


public class ItemDataModel
{
	public static event ItemUnequipped OnItemUnequipped;
	public delegate void ItemUnequipped(ItemCellData data);

	private List<ItemCellData> items_ = new List<ItemCellData>();
	public List<ItemCellData> Items => this.items_;

	private Dictionary<string, ItemCellData> equippedItems_ =
		new Dictionary<string, ItemCellData>();

	public ItemDataModel()
	{
		ItemCell.OnItemEquipped += (ItemCellData data) => this.EquipItem(data);
	}

	public void Add(ItemCellData data)
	{
		items_.Add(data);
	}

	public List<BaseCellData> Data()
	{
		List<BaseCellData> baseData = new List<BaseCellData>(this.items_.Count);
		foreach (ItemCellData itemCellData in this.items_)
			baseData.Add(itemCellData);
		return baseData;
	}

	private void EquipItem(ItemCellData data)
	{
		string subcategoryName = data.SubcategoryName.Value;
		if (this.equippedItems_.ContainsKey(subcategoryName))
		{
			// Equipped item got unequipped, no need to do anything
			if (data == this.equippedItems_[subcategoryName])
			{
				this.equippedItems_.Remove(subcategoryName);
			}
			// Different item got equipped, previously equipped item has to be informed
			else
			{
				ItemDataModel.OnItemUnequipped.Invoke(equippedItems_[subcategoryName]);
				this.equippedItems_[subcategoryName] = data;
			}
		}
		// New item from a category has been equipped, just add to the list
		else
		{
			this.equippedItems_[subcategoryName] = data;
		}
	}
}
