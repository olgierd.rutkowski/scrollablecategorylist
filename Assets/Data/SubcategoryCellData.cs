﻿public class SubcategoryCellData : BaseCellData
{
	public TextData titleData_;

	public SubcategoryCellData(string categoryName, string subcategoryName) :
		base(CellTypeEnum.Subcategory, categoryName, subcategoryName)
	{
		this.titleData_ = new TextData(subcategoryName);
	}

	public override bool ContainsPhrase(string phrase)
	{
		return base.ContainsPhrase(phrase) || this.titleData_.text_.ToLower().Contains(phrase);
	}

	public override string SortingPhrase()
	{
		return this.titleData_.text_;
	}
}
