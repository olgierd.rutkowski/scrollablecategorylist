﻿public class CategoryCellData : BaseCellData
{
	public TextData titleData_ = null;
	public ToggleButtonData collapseButtonData_ = null;

	public bool isCollapsed_ = false;

	public CategoryCellData(string categoryName) :
		base(CellTypeEnum.Category, categoryName, new Optional<string>())
	{
		this.titleData_ = new TextData(categoryName);
	}

	public override bool ContainsPhrase(string phrase)
	{
		return base.ContainsPhrase(phrase) || this.titleData_.text_.ToLower().Contains(phrase);
	}

	public override string SortingPhrase()
	{
		return this.titleData_.text_;
	}
}
