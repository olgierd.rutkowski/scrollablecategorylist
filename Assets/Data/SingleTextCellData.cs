﻿

public class SingleTextCellData : BaseCellData
{
	public TextData textData_ = null;

	public SingleTextCellData(string categoryName, Optional<string> subcategoryName) :
		base(CellTypeEnum.SingleText, categoryName, subcategoryName)
	{}

	public override bool ContainsPhrase(string phrase)
	{
		return base.ContainsPhrase(phrase) || this.textData_.text_.ToLower().Contains(phrase);
	}

	public override string SortingPhrase()
	{
		return this.textData_.text_;
	}
}
