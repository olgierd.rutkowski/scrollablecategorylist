﻿using System.Collections.Generic;
using UnityEngine;


public class ItemizerDataProvider : DataProvider
{
	public TextAsset jsonString_ = null;

	private ItemDataModel dataModel_ = new ItemDataModel();

	private new void Start()
	{
		base.Start();

		ItemsData itemsData = ItemsData.CreateFromJson(this.jsonString_.text);
		foreach (ItemData itemData in itemsData.items)
		{
			ItemCellData itemCellData = new ItemCellData(
				itemData.categoryName,
				itemData.subcategoryName
			);
			itemCellData.rarityTextData_ = new TextData(itemData.itemRarity);
			itemCellData.itemNameTextData_ = new TextData(itemData.itemName);
			this.dataModel_.Add(itemCellData);
		}
	}

	protected override List<BaseCellData> GenerateData()
	{
		return this.dataModel_.Data();
	}
}
