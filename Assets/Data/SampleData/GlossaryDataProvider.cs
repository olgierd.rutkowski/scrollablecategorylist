﻿using UnityEngine;
using System.Collections.Generic;


public class GlossaryDataProvider : DataProvider
{
	static private readonly string livingRoomCategory_ = "Living Room";
	static private readonly string kitchenCategory_ = "Kitchen";
	static private readonly string bathroomCategory_ = "Bathroom";
	static private readonly string bedroomCategory_ = "Bedroom";
	static private readonly string garageCategory_ = "Garage";

	static private readonly string charactersSubcategory_ = "Characters";
	static private readonly string eventsSubcategory_ = "Events";
	static private readonly string poiSubcategory_ = "Point of interest";

	/// <summary>
	///	Living Room
	///		Characters
	///			Guests
	///			Family
	///		Events
	///			Birthday
	///		Points of interest
	///			Cake
	///	Kitchen
	///		Characters
	///			Husband
	///			Wife
	///		Events
	///			Preparing food
	///		Points of interest
	///			Fridge
	///	Bathroom
	///		Characters
	///			Birthday child
	///		Events
	///			Cleaning cake covered fingers
	///		Points of interest
	///			Sink
	///	Bedroom
	///		Characters
	///			Sibling
	///			Friend
	///		Events
	///			Preparing birthday present
	///		Points of interest
	///			Wardrobe
	///			Wrapping paper
	///	Garage
	///		Characters
	///			Dog
	///		Events
	///			Playing with ball
	///		Points of interest
	///			Ball dented car
	///			Toolbox
	/// </summary>
	protected override List<BaseCellData> GenerateData()
	{
		List<BaseCellData> listData = new List<BaseCellData>(20);

		#region Living Room

		var guests = new SingleTextCellData(
			GlossaryDataProvider.livingRoomCategory_,
			GlossaryDataProvider.charactersSubcategory_
		);
		guests.textData_ = new TextData("Guests");
		listData.Add(guests);

		var family = new SingleTextCellData(
			GlossaryDataProvider.livingRoomCategory_,
			GlossaryDataProvider.charactersSubcategory_
		);
		family.textData_ = new TextData("Family");
		listData.Add(family);

		var birthday = new SingleTextCellData(
			GlossaryDataProvider.livingRoomCategory_,
			GlossaryDataProvider.eventsSubcategory_
		);
		birthday.textData_ = new TextData("Birthday");
		listData.Add(birthday);

		var cake = new SingleTextCellData(
			GlossaryDataProvider.livingRoomCategory_,
			GlossaryDataProvider.poiSubcategory_
		);
		cake.textData_ = new TextData("Cake");
		listData.Add(cake);

		#endregion Living Room

		#region Kitchen

		var husband = new SingleTextCellData(
			GlossaryDataProvider.kitchenCategory_,
			GlossaryDataProvider.charactersSubcategory_
		);
		husband.textData_ = new TextData("Husband");
		listData.Add(husband);

		var wife = new SingleTextCellData(
			GlossaryDataProvider.kitchenCategory_,
			GlossaryDataProvider.charactersSubcategory_
		);
		wife.textData_ = new TextData("Wife");
		listData.Add(wife);

		var preparingFood = new SingleTextCellData(
			GlossaryDataProvider.kitchenCategory_,
			GlossaryDataProvider.eventsSubcategory_
		);
		preparingFood.textData_ = new TextData("Preparing food");
		listData.Add(preparingFood);

		var fridge = new SingleTextCellData(
			GlossaryDataProvider.kitchenCategory_,
			GlossaryDataProvider.poiSubcategory_
		);
		fridge.textData_ = new TextData("Fridge");
		listData.Add(fridge);

		#endregion Kitchen

		#region Bathroom

		var birthdayChild = new SingleTextCellData(
			GlossaryDataProvider.bathroomCategory_,
			GlossaryDataProvider.charactersSubcategory_
		);
		birthdayChild.textData_ = new TextData("Birthday child");
		listData.Add(birthdayChild);

		var cleaningCakeCoveredFingers = new SingleTextCellData(
			GlossaryDataProvider.bathroomCategory_,
			GlossaryDataProvider.eventsSubcategory_
		);
		cleaningCakeCoveredFingers.textData_ = new TextData("Cleaning cake covered fingers");
		listData.Add(cleaningCakeCoveredFingers);

		var sink = new SingleTextCellData(
			GlossaryDataProvider.bathroomCategory_,
			GlossaryDataProvider.poiSubcategory_
		);
		sink.textData_ = new TextData("Sink");
		listData.Add(sink);

		#endregion Bathroom

		#region Bedroom

		var sibling = new SingleTextCellData(
			GlossaryDataProvider.bedroomCategory_,
			GlossaryDataProvider.charactersSubcategory_
		);
		sibling.textData_ = new TextData("Sibling");
		listData.Add(sibling);

		var friend = new SingleTextCellData(
			GlossaryDataProvider.bedroomCategory_,
			GlossaryDataProvider.charactersSubcategory_
		);
		friend.textData_ = new TextData("Friend");
		listData.Add(friend);

		var preparingBirthdayPresent = new SingleTextCellData(
			GlossaryDataProvider.bedroomCategory_,
			GlossaryDataProvider.eventsSubcategory_
		);
		preparingBirthdayPresent.textData_ = new TextData("Preparing birthday present");
		listData.Add(preparingBirthdayPresent);

		var wardrobe = new SingleTextCellData(
			GlossaryDataProvider.bedroomCategory_,
			GlossaryDataProvider.poiSubcategory_
		);
		wardrobe.textData_ = new TextData("Wardrobe");
		listData.Add(wardrobe);

		var wrappingPaper = new SingleTextCellData(
			GlossaryDataProvider.bedroomCategory_,
			GlossaryDataProvider.poiSubcategory_
		);
		wrappingPaper.textData_ = new TextData("Wrapping paper");
		listData.Add(wrappingPaper);

		#endregion Bedroom

		#region Garage

		var dog = new SingleTextCellData(
			GlossaryDataProvider.garageCategory_,
			GlossaryDataProvider.charactersSubcategory_
		);
		dog.textData_ = new TextData("Dog");
		listData.Add(dog);

		var playingWithBall = new SingleTextCellData(
			GlossaryDataProvider.garageCategory_,
			GlossaryDataProvider.eventsSubcategory_
		);
		playingWithBall.textData_ = new TextData("Playing with ball");
		listData.Add(playingWithBall);

		var ballDentedCar = new SingleTextCellData(
			GlossaryDataProvider.garageCategory_,
			GlossaryDataProvider.poiSubcategory_
		);
		ballDentedCar.textData_ = new TextData("Ball dented car");
		listData.Add(ballDentedCar);

		var toolbox = new SingleTextCellData(
			GlossaryDataProvider.garageCategory_,
			GlossaryDataProvider.poiSubcategory_
		);
		toolbox.textData_ = new TextData("Toolbox");
		listData.Add(toolbox);

		#endregion Garage

		return listData;
	}
}
