﻿using System;
using UnityEngine;


[Serializable]
public class ItemsData
{
	public ItemData[] items;

	public static ItemsData CreateFromJson(string jsonString)
	{
		return JsonUtility.FromJson<ItemsData>(jsonString);
	}
}

[Serializable]
public class ItemData
{
	public string categoryName;
	public string subcategoryName;
	public string itemRarity;
	public string itemName;

	public static ItemData CreateFromJson(string jsonString)
	{
		return JsonUtility.FromJson<ItemData>(jsonString);
	}
}
