﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;


public class SubcategoryCell : BaseCell
{
	public Text title_ = null;

	public SubcategoryCell() :
		base(CellTypeEnum.Subcategory)
	{}

	public override void ApplyData(BaseCellData data)
	{
		base.ApplyData(data);

		if (!(data is SubcategoryCellData))
			return;

		SubcategoryCellData subcategoryCellData = (SubcategoryCellData)data;
		UI.ApplyTextData(this.title_, subcategoryCellData.titleData_);
	}

	private new void Start()
	{
		base.Start();

		Assert.IsNotNull(this.title_, "Title has to be provided for SubcategoryCell.");
	}
}
