﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;


public class SingleTextCell : BaseCell
{
	public Text text_ = null;

	public SingleTextCell() :
		base(CellTypeEnum.SingleText)
	{}

	public override void ApplyData(BaseCellData data)
	{
		base.ApplyData(data);

		if (!(data is SingleTextCellData))
			return;

		SingleTextCellData singleTextCellData = (SingleTextCellData)data;
		UI.ApplyTextData(this.text_, singleTextCellData.textData_);
	}

	private new void Start()
	{
		base.Start();

		Assert.IsNotNull(this.text_, "Text has to be supplied for SingleTextCell.");
	}
}
