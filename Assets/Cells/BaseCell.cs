﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;


public class BaseCell : MonoBehaviour
{
	public string categoryName_ = null;
	public Optional<string> subcategoryName_ = null;

	public RectTransform bounds_ = null;
	public Image background_ = null;

	protected List<GameObject> subcells_ = new List<GameObject>();

	private readonly CellTypeEnum cellType_ = CellTypeEnum.Unknown;
	public CellTypeEnum CellType => this.cellType_;

	private BaseCellData baseCellData_ = null;
	public BaseCellData BaseCellData => this.baseCellData_;

	public BaseCell(CellTypeEnum cellType)
	{
		Assert.AreNotEqual(CellTypeEnum.Unknown, cellType, "Cells can't be of unknown type.");
		this.cellType_ = cellType;
	}

	public virtual void ApplyData(BaseCellData data)
	{
		Assert.IsNotNull(data, "Base data can't be null when passed to BaseCell.");

		this.categoryName_ = data.CategoryName;
		this.subcategoryName_ = data.SubcategoryName;
		this.baseCellData_ = data;
		if (data.backgroundData_.HasValue)
			UI.ApplyImageData(this.background_, data.backgroundData_.Value);
	}

	public void AddSubcell(GameObject cellObject)
	{
		if (this.HasSubcell(cellObject))
			return;
		this.subcells_.Add(cellObject);
	}

	public void RemoveSubcell(GameObject cellObject)
	{
		this.subcells_.Remove(cellObject);
	}

	public bool HasSubcell(GameObject cellObject)
	{
		return this.subcells_.Contains(cellObject);
	}

	public void RemoveAllSubcells()
	{
		this.subcells_.Clear();
	}

	public Optional<GameObject> SubcellFor(CellTypeEnum cellType, string name)
	{
		foreach (GameObject gameObject in this.subcells_)
		{
			var cell = gameObject.GetComponent<BaseCell>();
			var cellName = cell.CellName();
			if (cell.cellType_ == cellType && cellName.HasValue && name == cellName.Value)
				return gameObject;
		}
		return new Optional<GameObject>();
	}

	public List<GameObject> Subcells()
	{
		return this.subcells_;
	}

	protected void Start()
	{
		Assert.IsNotNull(this.bounds_, "Bounds have to be specified for a base cell.");
		Assert.IsNotNull(this.background_, "Background has to be specified for a base cell.");
	}

	private Optional<string> CellName()
	{
		switch(this.CellType)
		{
			case CellTypeEnum.Category:
				return this.categoryName_;
			case CellTypeEnum.Subcategory:
				return this.subcategoryName_;
		}
		return new Optional<string>();
	}
}
