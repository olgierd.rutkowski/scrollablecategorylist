﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;


public class TripleTextCell : BaseCell
{
    public Text leftText_ = null;
    public Text middleText_ = null;
    public Text rightText_ = null;
    public Image image_ = null;

    public TripleTextCell() :
        base(CellTypeEnum.TripleText)
    {}

    public override void ApplyData(BaseCellData data)
	{
        base.ApplyData(data);

        if (!(data is TripleTextCellData))
            return;

        TripleTextCellData tripleTextCellData = (TripleTextCellData)data;
        if (tripleTextCellData.leftTextData_.HasValue)
            UI.ApplyTextData(this.leftText_, tripleTextCellData.leftTextData_.Value);
        if (tripleTextCellData.middleTextData_.HasValue)
            UI.ApplyTextData(this.middleText_, tripleTextCellData.middleTextData_.Value);
        if (tripleTextCellData.rightTextData_.HasValue)
            UI.ApplyTextData(this.rightText_, tripleTextCellData.rightTextData_.Value);
        if (tripleTextCellData.imageData_.HasValue)
            UI.ApplyImageData(this.image_, tripleTextCellData.imageData_.Value);
	}

	private new void Start()
    {
        base.Start();

        Assert.IsNotNull(this.leftText_, "Left textfield has to be supplied for TripleTextCell.");
        Assert.IsNotNull(this.middleText_, "Middle textfield has to be supplied for TripleTextCell.");
        Assert.IsNotNull(this.rightText_, "Right textfield has to be supplied for TripleTextCell.");
        // Image is optional, thus no assertion.
    }
}
