﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;


public class ItemCell : BaseCell
{
	public static event ItemEquipped OnItemEquipped;
	public delegate void ItemEquipped(ItemCellData data);

	public Text rarityText_ = null;
	public Text itemNameText_ = null;
	public Image equippedIcon_ = null;
	public Button equipButton_ = null;

	private Text equipButtonText_ = null;

	private ItemCellData data_ = null;

	public ItemCell() :
		base(CellTypeEnum.Item)
	{}

	public override void ApplyData(BaseCellData data)
	{
		base.ApplyData(data);

		if (!(data is ItemCellData))
			return;

		this.equippedIcon_.enabled = false;

		this.data_ = (ItemCellData)data;
		UI.ApplyTextData(this.rarityText_, this.data_.rarityTextData_);
		UI.ApplyTextData(this.itemNameText_, this.data_.itemNameTextData_);
	}

	private new void Start()
	{
		base.Start();

		Assert.IsNotNull(this.rarityText_, "Rarity text has to be specified for ItemCell.");
		Assert.IsNotNull(this.itemNameText_, "Item name text has to be specified for ItemCell");
		Assert.IsNotNull(this.equippedIcon_, "Equipped icon has to be specified for ItemCell.");
		Assert.IsNotNull(this.equipButton_, "Equip button has to be specified for ItemCell.");

		this.equipButton_.onClick.AddListener(OnEquipButtonClicked);
		this.equipButtonText_ = this.equipButton_.transform.GetChild(0).GetComponent<Text>();
		Assert.IsNotNull(this.equipButtonText_, "Equip button has to have text field in ItemCell.");

		ItemDataModel.OnItemUnequipped += (ItemCellData data) => this.OnItemUnequipped(data);
	}

	private void OnEquipButtonClicked()
	{
		this.equippedIcon_.enabled = !this.equippedIcon_.enabled;
		this.equipButtonText_.text = this.equippedIcon_.enabled ? "Unequip" : "Equip";
		ItemCell.OnItemEquipped.Invoke(this.data_);
	}

	private void OnItemUnequipped(ItemCellData data)
	{
		if (this.data_ != data)
			return;

		if (this.equippedIcon_)
			this.equippedIcon_.enabled = false;
		if (this.equipButton_)
			this.equipButtonText_.text = "Equip";
	}
}
