﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;


public class CategoryCell : BaseCell
{
	public static event CategoryStateChange OnCategoryStateChange;
	public delegate void CategoryStateChange();

	public Text title_ = null;
	public Button collapseButton_ = null;
	public ToggleButtonData collapseButtonData_ = null;

	private CategoryCellData categoryCellData_ = null;
	public CategoryCellData CategoryCellData => this.categoryCellData_;

	public CategoryCell() :
		base(CellTypeEnum.Category)
	{}

	public override void ApplyData(BaseCellData data)
	{
		base.ApplyData(data);

		if (!(data is CategoryCellData))
			return;

		this.categoryCellData_ = (CategoryCellData)data;
		UI.ApplyTextData(this.title_, categoryCellData_.titleData_);
	}

	private new void Start()
	{
		base.Start();

		Assert.IsNotNull(this.title_, "Title has to be provided for CategoryCell.");
		Assert.IsNotNull(this.collapseButton_, "Button has to be provided for CategoryCell.");
		Assert.IsNotNull(
			this.collapseButtonData_,
			"Button data has to be provided for CategoryCell."
		);

		this.collapseButton_.image.sprite = this.DetermineButtonSprite();
		this.collapseButton_.onClick.AddListener(this.CollapseButtonClicked);
	}

	private void CollapseButtonClicked()
	{
		this.categoryCellData_.isCollapsed_ = !this.categoryCellData_.isCollapsed_;
		this.collapseButton_.image.sprite = this.DetermineButtonSprite();
		CategoryCell.OnCategoryStateChange.Invoke();
	}

	private Sprite DetermineButtonSprite()
	{
		if (this.categoryCellData_.isCollapsed_)
			return this.collapseButtonData_.toggled_;
		else
			return this.collapseButtonData_.untoggled_;
	}
}
