﻿public class EnumToString
{
	static public string CellTypeEnumToString(CellTypeEnum cellType)
	{
		switch(cellType)
		{
			case CellTypeEnum.Category:
				return "Category";
			case CellTypeEnum.SingleText:
				return "Single Text";
			case CellTypeEnum.Subcategory:
				return "Subcategory";
			case CellTypeEnum.TripleText:
				return "Triple Text";
			case CellTypeEnum.Item:
				return "Item";
		}
		return "Unknown";
	}

	static public string SortingMethodEnumToString(SortingMethodEnum sortingMethod)
	{
		switch(sortingMethod)
		{
			case SortingMethodEnum.Alphabetical:
				return SortingMethodStrings.Alphabetical;
			case SortingMethodEnum.None:
				return SortingMethodStrings.None;
			case SortingMethodEnum.ReverseAlphabetical:
				return SortingMethodStrings.ReverseAlphabetical;
		}
		return SortingMethodStrings.Unknown;
	}
}
