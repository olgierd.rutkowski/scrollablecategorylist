﻿public class SortingMethodStrings
{
	static public readonly string None = "Sort: None";
	static public readonly string Alphabetical = "Sort: Alphabetical";
	static public readonly string ReverseAlphabetical = "Sort: Reverse Alphabetical";
	static public readonly string Unknown = "Sort: Unknown";
}

public enum SortingMethodEnum
{
	None,
	Alphabetical,
	ReverseAlphabetical,
	Unknown
}
