﻿public class StringToEnum
{
	public static SortingMethodEnum SortingMethodStringToEnum(string sortingMethod)
	{
		if (sortingMethod == SortingMethodStrings.None)
			return SortingMethodEnum.None;
		if (sortingMethod == SortingMethodStrings.Alphabetical)
			return SortingMethodEnum.Alphabetical;
		if (sortingMethod == SortingMethodStrings.ReverseAlphabetical)
			return SortingMethodEnum.ReverseAlphabetical;
		return SortingMethodEnum.Unknown;
	}
}
