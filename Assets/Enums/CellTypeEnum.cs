﻿/// <summary>
/// Enumeration describing possible state types.
/// </summary>
public enum CellTypeEnum
{
	SingleText,
	TripleText,
	Item,
	Category,
	Subcategory,
	// Only used for comparison and initialization in the BaseCell
	Unknown
}
