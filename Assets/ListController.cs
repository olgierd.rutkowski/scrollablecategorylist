﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections.Generic;


public class ListController : MonoBehaviour
{
	[SerializeField]
	private DataProvider dataProvider_ = null;

	[SerializeField]
	private ScrollWidgetController scrollWidgetController_ = null;

	[SerializeField]
	private GameObject viewport_ = null;

	[SerializeField]
	private GameObject categoryCellFactory_ = null;

	[SerializeField]
	private GameObject subcategoryCellFactory_ = null;

	[SerializeField]
	private List<GameObject> cellFactories_ = null;

	[SerializeField]
	private Transform cellSpawnPoint_ = null;

	[SerializeField]
	private RectTransform cellContent_ = null;

	[SerializeField]
	private float contentMargin_ = 0.0f;

	[SerializeField]
	private float categoryPadding_ = 0.0f;

	[SerializeField]
	private float subcategoryPadding_ = 0.0f;

	[SerializeField]
	private float cellPadding_ = 0.0f;

	public bool showSubcategories_ = true;

	private bool isInitialized = false;
	private bool isRerenderScheduled = false;
	private List<BaseCellData> data_ = null;
	private List<GameObject> cells_ = new List<GameObject>();
	private Dictionary<CellTypeEnum, GameObject> categorizedCellFactories_ = null;
	private Dictionary<string, CategoryCell> categoryCells_ = null;

	private Dictionary<string, CategoryCellData> categoryCellData_ = null;
	private Dictionary<string, SubcategoryCellData> subcategoryCellData_ = null;

	private void Start()
	{
		Assert.IsNotNull(
			this.dataProvider_,
			"Data provider must be specified for ListController."
		);
		Assert.IsNotNull(
			this.categoryCellFactory_,
			"Category cell factory has to be specified for ListController."
		);
		Assert.IsNotNull(
			this.cellFactories_,
			"Cell factories must be specified for ListController."
		);
		Assert.IsTrue(
			this.cellFactories_.Count > 0,
			"At least one cell factory must exist for ListController."
		);
		Assert.IsNotNull(
			this.cellSpawnPoint_,
			"Spawn point has to be specified for ListController."
		);
		Assert.IsNotNull(
			this.cellContent_,
			"Cell content has to be specified for ListController."
		);

		if (this.scrollWidgetController_)
		{
			this.scrollWidgetController_.scrollTo_.onClick.AddListener(this.ScrollTo);
		}

		this.categorizedCellFactories_ = new Dictionary<CellTypeEnum, GameObject>(
			// Magical two is caused by category and subcategory factories.
			this.cellFactories_.Count + 2
		);
		foreach (GameObject gameObject in this.cellFactories_)
		{
			BaseCell baseCell1 = gameObject.GetComponent<BaseCell>();
			this.categorizedCellFactories_[baseCell1.CellType] = gameObject;
		}
		this.categorizedCellFactories_[CellTypeEnum.Category] = this.categoryCellFactory_;
		this.categorizedCellFactories_[CellTypeEnum.Subcategory] = this.subcategoryCellFactory_;

		this.categoryCellData_ = new Dictionary<string, CategoryCellData>();
		this.subcategoryCellData_ = new Dictionary<string, SubcategoryCellData>();

		CategoryCell.OnCategoryStateChange += () => this.OnCategoryStateChange();
		this.dataProvider_.OnDataChanged += () => this.isRerenderScheduled = true;

		this.isRerenderScheduled = true;
		this.isInitialized = true;
	}

	private void Update()
	{
		if (this.isRerenderScheduled)
		{
			this.RenderList(true);
			this.isRerenderScheduled = false;
		}
	}

	private void OnValidate()
	{
		if (this.isInitialized)
		{
			this.isRerenderScheduled = true;
		}
	}

	private void RenderList(bool refreshData)
	{
		//	TODO: cell pooling should be introduced here.
		this.cellSpawnPoint_.DetachChildren();
		foreach (GameObject cell in this.cells_)
			Destroy(cell);
		this.cells_.Clear();

		if (refreshData)
		{
			this.data_ = this.dataProvider_.ListData();
			this.categoryCells_ = new Dictionary<string, CategoryCell>();
		}

		float currentSpawnHeight = this.contentMargin_;
		currentSpawnHeight += this.RenderCells(this.SortCells(), currentSpawnHeight);
		this.cellContent_.sizeDelta = new Vector2(0.0f, currentSpawnHeight + this.contentMargin_);
	}

	private float RenderCells(List<GameObject> cells, float currentSpawnHeight)
	{
		foreach (GameObject cell in cells)
		{
			BaseCell baseCell = cell.GetComponent<BaseCell>();
			float padding = this.CalculatePaddingForCell(baseCell);
			if (ShouldRenderCell(baseCell))
			{
				cell.transform.position = new Vector3(
					0.0f,
					-currentSpawnHeight - padding,
					0.0f
				);
				cell.transform.SetParent(this.cellSpawnPoint_, false);

				var cellBounds = cell.GetComponent<RectTransform>();
				currentSpawnHeight += padding * 2.0f + cellBounds.rect.height;
			}

			this.cells_.Add(cell);
			if (baseCell.Subcells().Count > 0 && this.ShouldRenderSubcells(baseCell))
				currentSpawnHeight = this.RenderCells(baseCell.Subcells(), currentSpawnHeight);
		}
		return currentSpawnHeight;
	}

	private List<GameObject> SortCells()
	{
		Dictionary<string, GameObject> categoryCells = new Dictionary<string, GameObject>();
		foreach (BaseCellData data in this.data_)
		{
			CategoryCell categoryCell;
			if (!categoryCells.ContainsKey(data.CategoryName))
			{
				if (!this.categoryCellData_.ContainsKey(data.CategoryName))
				{
					this.categoryCellData_[data.CategoryName] =
						new CategoryCellData(data.CategoryName);
				}

				CategoryCellData categoryCellData = this.categoryCellData_[data.CategoryName];
				GameObject categoryCellObject = this.CreateCell(categoryCellData);
				categoryCells[data.CategoryName] = categoryCellObject;
			}
			categoryCell = categoryCells[data.CategoryName].GetComponent<CategoryCell>();

			if (data.SubcategoryName.HasValue)
			{
				Optional<GameObject> subcategoryCellObject = categoryCell.SubcellFor(
					CellTypeEnum.Subcategory,
					data.SubcategoryName.Value
				);
				if (!subcategoryCellObject.HasValue)
				{
					if (!this.subcategoryCellData_.ContainsKey(data.SubcategoryName.Value))
					{
						this.subcategoryCellData_[data.SubcategoryName.Value] =
							new SubcategoryCellData(data.CategoryName, data.SubcategoryName.Value);
					}

					SubcategoryCellData subcategoryCellData =
						this.subcategoryCellData_[data.SubcategoryName.Value];
					subcategoryCellObject = this.CreateCell(subcategoryCellData);
					categoryCell.AddSubcell(subcategoryCellObject.Value);
				}
				SubcategoryCell subcategoryCell
						= subcategoryCellObject.Value.GetComponent<SubcategoryCell>();
				GameObject cell = this.CreateCell(data);
				subcategoryCell.AddSubcell(cell);
			}
			else
			{
				categoryCell.AddSubcell(this.CreateCell(data));
			}
		}

		List<GameObject> cells = new List<GameObject>(categoryCells.Count);
		foreach (var category in categoryCells)
			cells.Add(category.Value);
		return cells;
	}

	private GameObject CreateCell(BaseCellData data)
	{
		GameObject categoryCellObject = Instantiate(
			this.categorizedCellFactories_[data.CellType],
			this.cellSpawnPoint_.transform.position,
			this.cellSpawnPoint_.rotation
		);
		// HACK: for now, but the map of possible Classes should be passed from outside.
		BaseCell baseCell = categoryCellObject.GetComponent<BaseCell>();
		if (baseCell is CategoryCell categoryCell)
		{
			this.categoryCells_[categoryCell.categoryName_] = categoryCell;
		}

		baseCell.ApplyData(data);
		return categoryCellObject;
	}

	private bool ShouldRenderCell(BaseCell cell)
	{
		return cell.CellType != CellTypeEnum.Subcategory
			|| (cell.CellType == CellTypeEnum.Subcategory && this.showSubcategories_);
	}

	private bool ShouldRenderSubcells(BaseCell cell)
	{
		if (!(cell is CategoryCell))
			return true;

		CategoryCell categoryCell = (CategoryCell)cell;
		return !categoryCell.CategoryCellData.isCollapsed_;
	}

	private void OnCategoryStateChange()
	{
		this.RenderList(false);
	}

	private float CalculatePaddingForCell(BaseCell cell)
	{
		float padding = this.cellPadding_;
		if (cell is CategoryCell)
			padding += this.categoryPadding_;
		else if (cell is SubcategoryCell)
			padding += this.subcategoryPadding_;
		return padding;
	}

	private void ScrollTo()
	{
		string phrase = this.scrollWidgetController_.input_.text;

		GameObject found = null;
		foreach (GameObject cell in this.cells_)
		{
			BaseCell baseCell = cell.GetComponent<BaseCell>();
			if (
				baseCell.BaseCellData.SortingPhrase().Equals(
					phrase,
					System.StringComparison.InvariantCultureIgnoreCase
				)
			)
			{
				found = cell;
				break;
			}
		}

		if (found)
		{
			Vector2 viewportLocalPosition = this.viewport_.transform.localPosition;
			Vector2 childLocalPosition = found.transform.localPosition;
			this.cellContent_.transform.localPosition = new Vector3(
				0.0f - (viewportLocalPosition.x + childLocalPosition.x),
				0.0f - (viewportLocalPosition.y + childLocalPosition.y),
				0.0f
			);
		}
	}
}
