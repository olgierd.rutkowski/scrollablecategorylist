# ScrollableCategoryList

This project has been done as part of recruitation process.

The project contains 2 scenes with examples (and an additional one with test subjects to ignore):

GlossaryExample - is a scene where the list has 20 items with areas being the categories.
ItemsExample - is a scene where items are being loaded from a JSON file. Only one item per subcategory can be equipped. Equipping another will unequip the other.
The project's been done using the newest version of the Unity framework - 2020.1.0f1.

The .exe file can be found in the [Deploy](https://gitlab.com/olgierd.rutkowski/scrollablecategorylist/-/tree/development/Deploy) folder.
Deployed project has been built from Main Scene.

According to the time tracker I have spent 17 hour implementing the project and around 4 hours planning it.
